#include "NetDragoRacer.h"

CNetDragoRacer::CNetDragoRacer()
{
	//SetNetworkIDManager(Singleton<NetworkManager>::GetInstance()->GetNetworkIDManager());
}

void CNetDragoRacer::Accelerate()
{
	__super::Accelerate();
	Singleton<NetworkManager>::GetInstance()->NetworkAccelerate(GetNetworkID());
}

void CNetDragoRacer::Decelerate()
{
	__super::Decelerate();
	//Singleton<NetworkManager>::GetInstance()->Decelerate(GetNetworkID());
}