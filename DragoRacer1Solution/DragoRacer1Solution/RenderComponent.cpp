#include "RenderComponent.h"
#include "Sprite.h"
#include "RenderingManager.h"
#include "Entity.h"
//load sprite
//add sprite to manager

CRenderComponent::CRenderComponent()
{
	m_Sprite = new Sprite();
	Singleton<RenderingManager>::GetInstance()->AddSprite(m_Sprite);
}

CRenderComponent::~CRenderComponent()
{
	Singleton<RenderingManager>::GetInstance()->RemoveSprite(m_Sprite);

	if(m_Sprite)
		delete m_Sprite;
}

void CRenderComponent::Init(std::string imageFileName)
{
	m_Sprite->LoadImage(imageFileName);
	Singleton<RenderingManager>::GetInstance()->AddSprite(m_Sprite);
}

void CRenderComponent::Update(float dt)
{
	if(m_Owner)
	{
		m_Sprite->SetPosition(m_Owner->GetPosition());
	}
}
