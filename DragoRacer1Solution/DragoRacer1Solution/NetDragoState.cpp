#include "NetDragoState.h"
#include "DragoGameManager.h"
#include "InputManager.h"
#include "RenderingManager.h"
#include "NetworkManager.h"
#include "AudioManager.h"
#include "Sprite.h"


NetDragoState::~NetDragoState()
{

}

void NetDragoState::Init()
{
	m_DragoGameManager = static_cast<DragoGameManager*>(m_GameManager);
	m_DragoGameManager->Init();

	bool drawBG = m_GameManager->GetNetworkManager()->IsHost() || m_IsClientReady;

	if(drawBG)
		m_DragoGameManager->DrawGameBG();

	//if you are host create the other players locally and send a packet to inform others
	if(m_GameManager->GetNetworkManager()->IsHost())
	{
		//host creates himself
		m_DragoGameManager->HostCreatePlayer();
		unsigned short numPlayers = 0;
		m_GameManager->GetNetworkManager()->GetNumPlayersInConnectionList(numPlayers);
		for(int i = 0; i < numPlayers; ++i)
		{
			//create the other players
			//host creates all players so that netid's will be controlled that way
			m_DragoGameManager->HostCreatePlayer();
		}
	}
}

bool NetDragoState::Update(float deltaTime)
{
	/*if(m_DesiredState == GameManager::GS_Join)
	{
		ChangeState(new JoinState(m_GameManager));
		return true;
	}
	else if(m_DesiredState == GameManager::GS_Host)
	{
		ChangeState(new HostState(m_GameManager));
		return true;
	}*/

	return false;
}

void NetDragoState::Shutdown()
{
	//m_GameManager->GetRenderingManager()->RemoveSprite(m_SplashScreen);
	//m_GameManager->GetAudioManager()->StopAll();
}

void NetDragoState::HandleKeyUp(int key)
{
	if(key == DIK_ESCAPE)
		m_DragoGameManager->SetIsGameOver(true);


	m_DragoGameManager->TestAccelerate(key,true);
}


void NetDragoState::HandleKeyDown(int key)
{
	m_DragoGameManager->TestAccelerate(key);
}

void NetDragoState::HandlePacket( RakNet::Packet *packet)
{
	switch (packet->data[0])
	{
	case NetworkManager::ID_CLIENT_CREATE_RACER:
		HandleClientCreateRacer(packet);
		break;
	case NetworkManager::ID_ACCELERATE:
		HandleAccelerate(packet);
		break;
	case ID_READY_EVENT_ALL_SET:
		m_IsClientReady = true;
		m_DragoGameManager->DrawGameBG();
		//HandleEventReady();
		break;
	case ID_READY_EVENT_SET:
		m_IsClientReady = true;
		m_DragoGameManager->DrawGameBG();
		//HandleEventReady();
		break;
	default:
		break;
	}
}

void NetDragoState::HandleClientCreateRacer(RakNet::Packet *packet)
{
	m_DragoGameManager->ClientCreatePlayer(packet);
}

void NetDragoState::HandleAccelerate(RakNet::Packet *packet)
{
	m_DragoGameManager->HandleAccelerate(packet);
}
