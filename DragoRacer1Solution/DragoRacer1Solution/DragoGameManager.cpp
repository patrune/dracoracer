#include "DragoGameManager.h"
#include "Sprite.h"
#include "DragoRacer.h"
#include "InputManager.h"
#include "RenderingManager.h"
#include "NetDragoRacer.h"
//#include "NetworkManager.h"

const float DragoGameManager::COUNTDOWNTIME = 2.0f;

DragoGameManager::DragoGameManager() : GameManager()
	,m_IsActive(true)
	,m_NumPlayers(0)
	,m_CountdownTimer(COUNTDOWNTIME)
	,m_Background(nullptr)
	,m_CountdownSprite(nullptr)
	,m_RaceResultSprite(nullptr)
{
}

DragoGameManager::~DragoGameManager()
{
	if(m_Background)
		delete m_Background;

	m_DragRacers.clear();
}

void DragoGameManager::Init()
{
	__super::Init();
	InitStartingPositions();
	InitInputKeys();
}

void DragoGameManager::Update(float dt)
{
	if(m_NumPlayers > 0 && m_CountdownTimer > FLT_EPSILON)
	{
		UpdateCountdownTimer(dt);
	}

	for(DRAGRACERSITERATOR it = m_DragRacers.begin();it!=m_DragRacers.end();++it)
	{
		(*it)->Update(dt);
	}

}

//MultiPlayer
void DragoGameManager::HostCreatePlayer()
{
	if(m_NumPlayers < MAXPLAYERS)
	{
		CNetDragoRacer *racer = new CNetDragoRacer();
		racer->Init();
		racer->SetNetworkIDManager(GetNetworkManager()->GetNetworkIDManager());
		racer->SetPosition(m_StartingPositions[m_NumPlayers]);
		m_DragRacers.push_back(racer);
		m_NumPlayers++;
		GetNetworkManager()->BroadcastCreateRacer(racer->GetNetworkID());
	}
}

void DragoGameManager::ClientCreatePlayer(RakNet::Packet *packet)
{
	RakNet::NetworkID netID = GetNetworkManager()->GetCreateRacerNetID(packet);
	CNetDragoRacer *racer = new CNetDragoRacer();
	racer->Init();
	racer->SetNetworkIDManager(GetNetworkManager()->GetNetworkIDManager());
	racer->SetNetworkID(netID); 
	racer->SetPosition(m_StartingPositions[m_NumPlayers]);
	m_DragRacers.push_back(racer);
	m_NumPlayers++;

}

void DragoGameManager::HandleAccelerate(RakNet::Packet *packet)
{
	RakNet::NetworkID netID = GetNetworkManager()->GetCreateRacerNetID(packet);
	CNetDragoRacer* racer = GetNetworkManager()->GetNetworkIDManager()->GET_OBJECT_FROM_ID<CNetDragoRacer*>(netID);
	if(racer)
	{
		racer->Accelerate();
	}
}

//Single Player
void DragoGameManager::SetNumPlayers(unsigned int numPlayers)
{
	m_NumPlayers = numPlayers;
	for(int i = 0;i < numPlayers;++i)
	{
		CDragoRacer *racer = new CDragoRacer();
		racer->Init();
		racer->SetPosition(m_StartingPositions[i]);
		m_DragRacers.push_back(racer);
	}
}

void DragoGameManager::DrawGameBG()
{
	m_Background = new Sprite();
	m_Background->LoadImage("dragtrack.png");
	m_Background->SetFullScreen();
	GetRenderingManager()->AddSprite(m_Background);
}

void DragoGameManager::InitStartingPositions()
{
	//take the screen height, divide by max number players
	//find the height of the image and leave that as a buffer
	float buffer = RenderingManager::SCREENHEIGHT/MAXPLAYERS + 100.0f;
	int i =0;
	for(int i = 0;i < MAXPLAYERS;++i)
	{
		m_StartingPositions.push_back(Vector2(0.0f,i*buffer));
	}
}

void DragoGameManager::InitInputKeys()
{
	static int keys[MAXPLAYERS*2] =
	{
		DIK_D,DIK_N,
		DIK_V,DIK_B,
		DIK_Q,DIK_W,
		DIK_O,DIK_P,
	};

	for(int i = 0;i < MAXPLAYERS;++i)
	{
		DragoInputKeys inputs;
		inputs.m_AccelerationKey = keys[i];
		inputs.m_NitroKey = keys[i+1];
		m_DragRacerInputKeys.push_back(inputs);
		i += 2;
	}
}

void DragoGameManager::UpdateCountdownTimer(float dt)
{
	//do a countdown
}

void DragoGameManager::drive()
{
	if(m_IsActive)
	{
		m_DragRacers[0]->Accelerate();
	}
}

bool DragoGameManager::TestAccelerate(int key, bool testDecelerate /* = false*/ )
{
	if(m_IsActive)
	{
		int i = 0;
		for(DRAGRACERSITERATOR it = m_DragRacers.begin();it!=m_DragRacers.end();++it,++i)
		{
			if(m_DragRacerInputKeys[i].m_AccelerationKey == key)
			{
				if(testDecelerate)
				{
					(*it)->Decelerate();
				}
				else
				{
					(*it)->Accelerate();

				}
			}
		}
	}
	return false;
}



bool DragoGameManager::TestNitro(int key)
{
	return false;
}