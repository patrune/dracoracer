#include "HostState.h"
#include "DragoGameManager.h"
#include "InputManager.h"
#include "RenderingManager.h"
#include "Sprite.h"
#include "NetworkManager.h"
#include "NetDragoState.h"
#include "BitStream.h"


void HostState::Init()
{
	//m_GameManager = Singleton<DragoGameManager>::GetInstance();

	m_SplashScreen = new Sprite();
	m_SplashScreen->LoadImage("Lundgren_Ivan_DragoHost.jpg");
	m_SplashScreen->SetFullScreen();
	m_GameManager->GetRenderingManager()->AddSprite(m_SplashScreen);

	m_StartText = new Sprite();
	m_StartText->LoadImage("StartText.png");
	m_StartText->SetVisible(false);
	m_StartText->SetPosition(Vector2(100.0f,300.0f));
	m_GameManager->GetRenderingManager()->AddSprite(m_StartText);

	m_Picture = std::vector<Sprite*>();
	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		Sprite* temp = new Sprite();
		temp->LoadImage("Go.png");
		temp->SetPosition(Vector2(temp->GetWidth()*i, temp->GetHeight()*i));
		temp->SetVisible(false);
		m_Picture.push_back(temp);
	}

	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		m_GameManager->GetRenderingManager()->AddSprite(m_Picture[i]);
	}

	netManager = m_GameManager->GetNetworkManager();
	netManager->InitHost();
	netManager->m_IsConnectionAccepted = false; 
	//netManager->AddPlayerToWaitList(netManager->getGUID());
}

void HostState::Shutdown()
{
	m_GameManager->GetRenderingManager()->RemoveSprite(m_SplashScreen);
	m_GameManager->GetRenderingManager()->RemoveSprite(m_StartText);
	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		m_GameManager->GetRenderingManager()->RemoveSprite(m_Picture[i]);
	}
}

void HostState::HandleKeyUp(int key)
{
	if(key == DIK_ESCAPE)
		m_GameManager->SetIsGameOver(true);

	if(key == DIK_S && CanStartGame())
	{
		m_DesiredState = GameManager::GS_NetDragoState;
	}

	if(netManager->m_IsConnectionAccepted){
		if(key == DIK_RETURN)
		{
			netManager->getReadyEvent().SetEvent(NetworkManager::RE_STARTGAME, true);	
		}
	}
}

bool HostState::Update(float deltaTime)
{
	if(m_DesiredState == GameManager::GS_NetDragoState)
	{

		m_GameManager->GetNetworkManager()->StartGame();
		
		ChangeState(new NetDragoState(m_GameManager));

		return true;
	}
	return false;
}

void HostState::HandleRequestAccepted(RakNet::Packet *packet)
{
	netManager->AddPlayerToWaitList(packet->guid);
	netManager->AddPlayerToWaitList(netManager->getGUID());
	netManager->m_IsConnectionAccepted = true;
	if(CanStartGame() && !m_StartText->GetVisible())
	{
		m_StartText->SetVisible(true);
	}
}

bool HostState::CanStartGame()
{
	unsigned short numPlayers;
	m_GameManager->GetNetworkManager()->GetNumPlayersInConnectionList(numPlayers);
	return numPlayers > 0;
}

void HostState::HandlePacket( RakNet::Packet *packet)
{
	switch (packet->data[0])
	{
		//case ID_CONNECTION_REQUEST_ACCEPTED:
		//		HandleRequestAccepted(packet);
		//	break;
	case ID_NEW_INCOMING_CONNECTION:
		HandleRequestAccepted(packet);

		break;
	case ID_DISCONNECTION_NOTIFICATION:
		// Connection lost normally
		//printf("ID_DISCONNECTION_NOTIFICATION\n");
		break;
	case ID_READY_EVENT_SET:
		for(int i = 0; i < netManager->getNumberPlayerInWaitList(); i++)
		{
			m_Picture[i]->SetVisible(true);
		}
		break;
	case ID_READY_EVENT_ALL_SET:
		break;
	default:
		break;
	}
}
