#include "SinglePlayerDragoState.h"
#include "DragoGameManager.h"
#include "InputManager.h"
#include "RenderingManager.h"
#include "NetworkManager.h"
#include "AudioManager.h"
#include "Sprite.h"


SinglePlayerDragoState::~SinglePlayerDragoState()
{

}

void SinglePlayerDragoState::Init()
{
	m_DragoGameManager = static_cast<DragoGameManager*>(m_GameManager);
	m_DragoGameManager->Init();
	m_DragoGameManager->DrawGameBG();
	m_DragoGameManager->SetNumPlayers(1);
	/*m_DesiredState = GameManager::GS_Intro;
	//m_GameManager = Singleton<DragoGameManager>::GetInstance();
	
	m_SplashScreen = new Sprite();
	m_SplashScreen->LoadImage("Lundgren_Ivan_DragoRacer.jpg");
	m_SplashScreen->SetFullScreen();
	m_GameManager->GetRenderingManager()->AddSprite(m_SplashScreen);

	m_AudioSample = m_GameManager->GetAudioManager()->Load("Rocky 8bit.mp3");
	if(m_AudioSample)
	{
		m_GameManager->GetAudioManager()->Play(m_AudioSample);
	}*/
}

bool SinglePlayerDragoState::Update(float deltaTime)
{
	/*if(m_DesiredState == GameManager::GS_Join)
	{
		ChangeState(new JoinState(m_GameManager));
		return true;
	}
	else if(m_DesiredState == GameManager::GS_Host)
	{
		ChangeState(new HostState(m_GameManager));
		return true;
	}*/

	return false;
}

void SinglePlayerDragoState::Shutdown()
{
	//m_GameManager->GetRenderingManager()->RemoveSprite(m_SplashScreen);
	//m_GameManager->GetAudioManager()->StopAll();
}

void SinglePlayerDragoState::HandleKeyUp(int key)
{
	if(key == DIK_ESCAPE)
		m_DragoGameManager->SetIsGameOver(true);

	m_DragoGameManager->TestAccelerate(key,true);
}


void SinglePlayerDragoState::HandleKeyDown(int key)
{
	m_DragoGameManager->TestAccelerate(key);
}
