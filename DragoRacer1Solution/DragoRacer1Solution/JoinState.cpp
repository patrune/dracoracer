#include "JoinState.h"

#include "DragoGameManager.h"
#include "InputManager.h"
#include "RenderingManager.h"
#include "Sprite.h"
#include "NetworkManager.h"
#include "AudioManager.h"
#include "IntroState.h"
#include "NetDragoState.h"

void JoinState::Init()
{

	netManager = m_GameManager->GetNetworkManager();
	netManager->InitClient();
	//m_GameManager = Singleton<DragoGameManager>::GetInstance();
	netManager->m_IsConnectionAccepted = false;

	LoadAssets();

	m_GameManager->GetRenderingManager()->AddSprite(m_SplashScreen);
	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		m_GameManager->GetRenderingManager()->AddSprite(m_Picture[i]);
	}

}

void JoinState::Shutdown()
{
	m_GameManager->GetRenderingManager()->RemoveSprite(m_SplashScreen);
	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		m_GameManager->GetRenderingManager()->RemoveSprite(m_Picture[i]);
	}
}

void JoinState::LoadAssets()
{
	m_SplashScreen = new Sprite();
	m_SplashScreen->LoadImage("Lundgren_Ivan_DragoJoin.jpg");
	m_SplashScreen->SetFullScreen();
	m_ConnectionSound = m_GameManager->GetAudioManager()->Load("boxing2rings.mp3");

	m_Picture = std::vector<Sprite*>();
	for(int i = 0; i < netManager->getMaxNumber(); i++)
	{
		Sprite* temp = new Sprite();
		temp->LoadImage("Go.png");
		temp->SetPosition(Vector2(temp->GetWidth()*i, temp->GetHeight()*i));
		temp->SetVisible(false);
		m_Picture.push_back(temp);
	}
}


void JoinState::HandleKeyUp(int key)
{
	if(key == DIK_ESCAPE)
		m_GameManager->SetIsGameOver(true);

	if(key == DIK_SPACE && !netManager->m_IsConnectionAccepted)
	{
		if(m_GameManager->GetNetworkManager()->EstablishConnection())
			m_DesiredState = GameManager::GS_NetDragoState;
	}

	if(netManager->m_IsConnectionAccepted){
		if(key == DIK_RETURN)
		{
			netManager->getReadyEvent().SetEvent(NetworkManager::RE_STARTGAME, true);	
		}
	}
}

bool JoinState::Update(float deltaTime)
{
	if(m_DesiredState == GameManager::GS_NetDragoState)
	{
		//ChangeState(new NetDragoState(m_GameManager));

		for(int i = 0; i < netManager->getNumberPlayerInWaitList(); i++)
		{
			m_Picture[i]->SetVisible(true);
		}
		return true;
	}
	return false;
}

void JoinState::HandleConnectionAccepted(RakNet::Packet *packet)
{
	m_GameManager->GetAudioManager()->Play(m_ConnectionSound);
	m_GameManager->GetNetworkManager()->SetHostAddress(packet->systemAddress);
	netManager->AddPlayerToWaitList(packet->guid);
	netManager->AddPlayerToWaitList(netManager->getGUID());
	netManager->m_IsConnectionAccepted = true;
}

void JoinState::HandlePacket( RakNet::Packet *packet)
{
	switch (packet->data[0])
	{
	case ID_CONNECTION_REQUEST_ACCEPTED:
		HandleConnectionAccepted(packet);
		break;
	case ID_READY_EVENT_SET:
		for(int i = 0; i < netManager->getNumberPlayerInWaitList(); i++)
		{
			m_Picture[i]->SetVisible(true);
		}
		break;
	case NetworkManager::ID_STARTGAME:
		ChangeState(new NetDragoState(m_GameManager));
		break;
	default:
		break;
	}
}


