#ifndef DRAGOGAMEMANAGER_H
#define DRAGOGAMEMANAGER_H

#include "GameManager.h"
#include <vector>
#include "CoreMath.h"

namespace RakNet
{
	class Packet;
}

class CDragoRacer;
class Sprite;

class DragoGameManager : public GameManager
{
public:
	struct DragoInputKeys
	{
		//customize input to test networking easier
		int m_AccelerationKey;
		int m_NitroKey;
	};

	DragoGameManager();
	virtual ~DragoGameManager();
	void Init();
	void Update(float dt);
	
	void DrawGameBG();

	//used for online
	void HostCreatePlayer();
	void ClientCreatePlayer(RakNet::Packet *packet);
	void HandleAccelerate(RakNet::Packet *packet);
	//used for sp 
	void SetNumPlayers(unsigned int numPlayers);

	bool TestAccelerate(int key,bool testDecelerate = false);
	bool TestNitro(int key);

	void InitInputKeys();
	void InitStartingPositions();
	void UpdateCountdownTimer(float dt);

	static const unsigned int MAXPLAYERS = 4;
	
	void drive();

private:
	static const float COUNTDOWNTIME;

	std::vector<DragoInputKeys> m_DragRacerInputKeys;
	std::vector<Vector2> m_StartingPositions;
	typedef std::vector<CDragoRacer*>::iterator DRAGRACERSITERATOR;
	std::vector<CDragoRacer*> m_DragRacers;
	Sprite* m_Background;
	Sprite* m_CountdownSprite;
	Sprite* m_RaceResultSprite;
	unsigned int m_NumPlayers;
	float m_CountdownTimer;
	bool m_IsActive;

};

#endif