#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include "Component.h"
#include <string>

class Sprite;

class CRenderComponent : public CComponent
{
public:
	CRenderComponent();
	~CRenderComponent();

	void Init(std::string imageFileName);
	void Update(float dt);

private:
	Sprite* m_Sprite;
protected:

};


#endif